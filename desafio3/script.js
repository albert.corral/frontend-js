/*
Vamos a construir un juego de test en la consola!
1. Construir un constructor Question que describa la pregunta. Debde incluir:
- question
- array de answers (u objeto)
- respuesta correcta
2 Crear un par de preguntas utilizando el constructor
3. Guardar las preguntas en un array
4. Seleccionar una pregunta aleatoria del array, y crear un método (mediante prototype) displayQuestion para pintarla por consola y a continuación las posibles respuestas (pista: para crear un número de pregunta aleatorio de un array: var n = Math.floor(Math.random() * questions.length);)
5. Usar prompt para que el usuario pueda elegir una opción. (pista: se debe cambiar el tipo del valor devuelto por prompt: parseInt)
6. Crear un método mediante prototype que corriga la respuesta e indique si hemos acertado o no.
*/

/*
1
*/
'use strict';

//Constructor Question
var Question = function (question, answers, answerOK){
    this.question = question;
    this.answers = answers;
    this.answerOK = answerOK;

};



    /*
    2 crear un par de instancias
    */
var question1 = new Question ('Capital de España',['Barcelona','Madrid','Roma','Londres'],2);
var question2 = new Question ('Color del caballo blanco de Santiago',['Rojo','Marrón','Blanco','Ceniza'],3);
var question3 = new Question ('Deporte más popular en España',['Baloncesto','Tenis','Fútbol','Squash'],3);

/*
3 crear array de preguntas

*/

var contest = [question1,question2,question3];

/*
4 crear método selectQuestion con prototype
*/

Question.prototype.displayQuestion = function(){
    console.log (this.question);
    //Mostrar opciones con ForEach
    // this.answers.forEach (function (element){
    //     console.log(element);
    // });
    this.answers.forEach((answer,index)=>{
        console.log(`${index+1}: ${answer}`);
    });

    // this.checkAnswer(userAnswer);

};

Question.prototype.checkAnswer = function(userAnswer){
   
    //comprobamos si la respuesta es correcta
    if (userAnswer===this.answerOK){
        alert('Respuesta correcta');
    }else{
        alert('Has fallado');
    }
};


//*******
// constructor methods
//************** */
var n = Math.floor(Math.random() * contest.length);
contest[n].displayQuestion();
var userAnswer=parseInt(prompt('Dame tu respuesta'));
contest[n].checkAnswer(userAnswer);


