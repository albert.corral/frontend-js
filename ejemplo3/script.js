'use strict';

//Funciones como argumentos

//Array fechas de nacimientos
var years = [1974,1990,1998,2002,2008,2006];

//Helped function

function calAge(year){
    return 2019 - year;
}

function isFullAge(age){
    return age > 17;
}

// Calcular ritmo cardíaco
function maxHeartRate(age){
    if (age >=18 && age <=81){
        return Math.round(206.9 - (0.67 * age));
    }
    else {
        return -1;
    }

}

//Main function
function arrayCalc(arr, fn){
    //Array for return a result
    var arrRes = [];
    for (let i = 0; i < arr.length; i++) {
        arrRes.push (fn (arr[i]));       
    }
    return arrRes;
}

//Calling
var ages = arrayCalc (years, calAge);
var fullAge = arrayCalc (ages, isFullAge);
var heartRate = arrayCalc(ages, maxHeartRate);

console.log("Edades:", ages);
console.log("Mayores de edad:", fullAge);

console.log("Máximas pulsaciones de edad:", heartRate);


 