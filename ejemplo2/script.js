'use strict';

//Creación de constructores y prototye. Object.create

//Obtener año actual
var date= new Date();
var year = date.getFullYear();




console.log('Fecha: ', year);
//Object.create
var personProto = {
    calcAge : function(){
        this.age = year-this.yearOfBirth;
    }
};
//Versión simple
var antonio = Object.create(personProto);
antonio.name = 'Antonio';

//Versión compleja
var maria = Object.create(personProto,
    {
        name : {value: 'Maria'},
        yearOfBirth : {value: 1980},
        job : {value: 'Designer'}
    });

maria.calcAge();
console.log('La edad de ', maria.name, 'es de ', maria.age);





 