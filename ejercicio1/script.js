'use strict';

//Creación de constructores y prototye

var Person = function (name, yearOfBirth, job){
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;

//creación de métodos
    this.calcAge = function (){
        this.age = 2019 - this.yearOfBirth;
       console.log (' La edad de '+ this.name+  ' es: '+ this.age);
    }
}
//Creamos instancia con método
var luis = new Person ('luis', 1987, 'Web Develop');
luis.calcAge();
console.log('Objecto con constructor después de llamar a método ', luis);

//constructor sin método, métodos mediante Prototype

Person.prototype.calcAge = function (){
    this.age = 2019 - this.yearOfBirth;
   console.log (' La edad de '+ this.name+  ' es: '+ this.age);
}

Person.prototype.lastName = 'Vicente';
var luis = new Person('Luis', 1987, 'teacher');

console.log('Objetos creados con constructor y PROTOTYPE ', luis);
luis.calcAge();
console.log('Objecto con constructor después de llamar a calAge ', luis);
console.log('Apellido obtenido con PROTOTYPE ', luis.lastName);