/*
  Una empresa que se dedica del cuidado y mantenimiento de los bosques, te pide un sofware para calcular la edad de diferentes especies de plantas.
  1. Crear una función que devuelta direrentes funciones para los casos:
  - Si es un pino, la edad será el diámetro (cm) dividido entre dos.
  - Si es un abeto, la edad será la (altura * diámetro)/ 10
  - Si es tomillo, será la altura * 5
  2. Utilizar las funciones creadas para cada caso.
*/
'use strict';

//Objeto árbol
var Tree = function (name, diameter, height){
    this.name = name;
    this.diameter = diameter;
    this.height = height;

//creación de métodos
    this.calcAge = function (){
        if (this.name === 'pino'){
            this.age = this.diameter /2;
        }
        else if (this.name === 'abeto'){
            this.age = (this.height * this.diameter)/10;
        }
        else if (this.name === 'tomillo'){
            this.age = this.height * 5;
        }
       console.log (' La edad de '+ this.name+  ' es: '+ this.age);
    }
}

var arbol1 = new Tree ('pino',34,48);
var arbol2 = new Tree ('abeto',45,48);
var arbol3 = new Tree ('tomillo',45,48);
arbol1.calcAge();
arbol2.calcAge();
arbol3.calcAge();

